const { Webo, WeboBook } = require("webo.js")
const fs = require("fs");

global.group = "Approweb"
global.name = "Input SP2DK"
global.version = "0.11"
global.data = "Approweb.xlsx"

const host = "http://approweb.intranet.pajak.go.id";

var webo = new Webo();
webo.parameters = function() {
    var workbook = new WeboBook(__dirname + "/" + global.data);
    var worksheet = workbook.sheet("DATA");

    var start = -1;
    var end = -1;
    for (var r = 5; r <= WeboBook.encodeRow(worksheet.maxRow); r++) {
        var number = parseInt(worksheet.value("A" + r));
        if (start == -1 || start > number)
            start = number;

        if (end == -1 || end < number)
            end = number;
    }

    var username = worksheet.value("B1");
    var password = worksheet.value("B2");

    return [{ name: "username", title:"Username", type: "text",  value: username? username : "" },
            { name: "password", title:"Password", type: "password",  value: password? password : "" },
            { name: "start", title:"Nomor Awal", type: "number",  value: start },
            { name: "end", title:"Nomor Terakhir", type: "number",  value: end }];
}

function main()
{
    webo.enableLog(__dirname + "/log.txt", "w+");
    var workbook = new WeboBook(__dirname + "/" + global.data);
    var worksheet = workbook.sheet("DATA");
    
    webo.load(host);
    webo.waitDocumentReady();

    do {
        webo.findElement("#LoginForm_ip").setAttribute("value", webo.arguments.username);
        webo.findElement("#LoginForm_kataSandi").setAttribute("value", webo.arguments.password);
        webo.findElement("button[type=submit]").click();
        webo.waitDocumentReady();

        if (webo.url().indexOf("r=home/index") == -1) {
            webo.log("Login Approweb Gagal!");
            break;
        }

        for (var r = 5; r <= WeboBook.encodeRow(worksheet.maxRow); r++) {
            var number = parseInt(worksheet.value("A" + r));
            if (isNaN(number) || number < parseInt(webo.arguments.start) || number > parseInt(webo.arguments.end))
                continue;

            var npwpFull = worksheet.value("B" + r);
            var jenisData = worksheet.value("C" + r);
            var jenisPajak = worksheet.value("D" + r);
            var nilaiPotensi = worksheet.value("E" + r);
            var uraianSingkat = worksheet.value("F" + r);
            var tahunPajak = worksheet.value("G" + r);
            var uraianSp2dk = worksheet.value("H" + r);
            var telp = worksheet.value("I" + r);
            var telp2 = worksheet.value("J" + r);
            var kertasKerja = worksheet.value("K" + r);
            var filePendukung = worksheet.value("L" + r);

            var errors = [];
            var npwpSearch = npwpFull.replace(/\./g, "").replace(/-/g, "").trim();
            if (!npwpSearch)
                errors.push("NPWP Kosong");
    
            if (!jenisData)
                errors.push("Jenis Data Kosong");
    
            if (!jenisPajak)
                errors.push("Jenis Pajak Kosong");
    
            if (!nilaiPotensi || isNaN(nilaiPotensi))
                errors.push("Nilai Potensi invalid");
    
            if (!uraianSingkat || uraianSingkat.length < 10)
                errors.push("Uraian Singkat kurang dari 10 karakter");
    
            if (!tahunPajak || isNaN(tahunPajak))
                errors.push("Tahun Pajak invalid");
    
            if (!uraianSp2dk)
                errors.push("Uraian SP2DK Kosong");
    
            if (!telp)
                errors.push("Telp Kosong");
    
            if (!kertasKerja || !fs.existsSync(kertasKerja))
                errors.push("Kertas Kerja kosong / tidak ditemukan");
    
            if (filePendukung && !fs.existsSync(filePendukung))
                errors.push("File Pendukung tidak ditemukan");
    
            if (errors.length) {
                webo.log("Nomor " + number + ": " + errors.join(", "));
                continue;
            }
    
            webo.load(host + "/index.php?r=home/cariWP");
            webo.waitDocumentReady();

            webo.findElement("#CariWPForm_kataCari").setAttribute("value", npwpSearch);
            webo.findElement("input[name=yt0]").click();
            webo.waitDocumentReady();

            var npwpTargetElm;
            webo.waitForElement("#tabCariWP > div.table-responsive");
            webo.findElement("#tabCariWP > div.table-responsive").findElements("tbody tr").forEach(function(tr, index, arr) {
                var tempElm = tr.findElement("a");
                var npwpTarget = tempElm.text().replace(/\./g, "").replace(/-/g, "").trim();
                if (npwpTarget == npwpSearch)
                    npwpTargetElm = tempElm;
            });

            if (!npwpTargetElm) {
                webo.log("Nomor " + number + ": NPWP Tidak ditemukan!");
                continue;
            }
    
            npwpTargetElm.click();
            webo.findElement("input[value='Buka profil']").click();
            webo.waitDocumentReady();
    
            if (webo.url().indexOf("r=permanen/ikhtisarWP") == -1) {
                webo.load(host + "/index.php?r=permanen/ikhtisarWP");
                webo.waitDocumentReady();
            }
    
            var isNpwpOk = false;
            webo.findElement(".portlet-body > table:nth-child(1) > tbody:nth-child(1)").findElements("tr").forEach(function(tr, index, arr) {
                var tdList = tr.findElements("td");
                if (tdList[0] && tdList[0].text().toLowerCase() == "npwp") {
                    if (tdList[2]) {
                        var npwpTarget = tdList[2].text().replace(/\./g, "").replace(/-/g, "");
                        if (npwpTarget == npwpSearch)
                            isNpwpOk = true;
                    }
                }
            });
    
            if (!isNpwpOk) {
                webo.log("Nomor " + number + ": Gagal membuka Profile!");
                continue;
            }

            webo.load(host + "/index.php?r=analisaData/potensi");
            webo.waitDocumentReady();

            webo.selectByValue("#Sp2dkPotensi_jenisdata", jenisData);
            webo.selectByValue("#Sp2dkPotensi_jenispajak", jenisPajak);
            webo.findElement("#Sp2dkPotensi_nipot").setAttribute("value", nilaiPotensi);
            webo.findElement("#Sp2dkPotensi_uraian").sendKeys(uraianSingkat);
            
            webo.findElement("button[type=submit]").click();
            webo.waitDocumentReady();

            var inputPotensi = false;
            if (webo.findElement("#output").findElement(".note").text() == "Sukses Input data")
                inputPotensi = true;
    
            if (inputPotensi == false) {
                webo.log("Nomor " + number + ": Input Potensi Gagal!");
                continue;
            }
    
            webo.load(host + "/index.php?r=analisaData/analisa");
            webo.waitDocumentReady();
    
            if (webo.findElement("#output").findElement("table").findElements("tbody tr").length <= 0) {
                webo.log("Nomor " + number + ": Potensi tidak ditemukan. Input Potensi gagal?");
                continue;
            }
    
            webo.findElement("#Sp2dkAnalisa_tahunpajak").setAttribute("value", tahunPajak);
            webo.findElement("#Sp2dkAnalisa_uraian").sendKeys(uraianSp2dk);
            webo.findElement("#Sp2dkAnalisa_telp1").setAttribute("value", telp);
            webo.findElement("#Sp2dkAnalisa_telp2").setAttribute("value", telp2? telp2 : "-");
            webo.findElement("#Sp2dkAnalisa_kertaskerja").sendKeys(kertasKerja);
            if (filePendukung)
                webo.findElement("#Sp2dkAnalisa_lainlain").sendKeys(filePendukung);
    
            webo.findElement(".form-actions a.btn.default").click();
            webo.waitForElement(".modal #btnClose", 20000);
    
            if (webo.findElement(".modal .modal-body").text().indexOf("Sukses") == -1) {
                webo.log("Nomor " + number + ": Tidak ditemukan keterangan Sukses. Input Analisa Data gagal? Cek pada Approweb karena potensi telah berhasil di rekam.");
                continue;
            }
    
            webo.log("Nomor " + number + ": Success!");
        }
    } while (false);

    webo.quit();
}

webo.start(main);