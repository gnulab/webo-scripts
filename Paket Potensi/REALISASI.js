const { Webo, WeboBook } = require("webo.js")
const fs = require("fs");

global.group = "Approweb"
global.name = "Input Realisasi"
global.version = "0.11"
global.data = "Approweb.xlsx"

const host = "http://approweb.intranet.pajak.go.id";

var webo = new Webo();
webo.parameters = function() {
    var workbook = new WeboBook(__dirname + "/" + global.data);
    var worksheet = workbook.sheet("DATA");

    var start = -1;
    var end = -1;
    for (var r = 5; r <= WeboBook.encodeRow(worksheet.maxRow); r++) {
        var number = parseInt(worksheet.value("A" + r));
        if (start == -1 || start > number)
            start = number;

        if (end == -1 || end < number)
            end = number;
    }

    var username = worksheet.value("B1");
    var password = worksheet.value("B2");

    return [{ name: "username", title:"Username", type: "text",  value: username? username : "" },
            { name: "password", title:"Password", type: "password",  value: password? password : "" },
            { name: "start", title:"Nomor Awal", type: "number",  value: start },
            { name: "end", title:"Nomor Terakhir", type: "number",  value: end }];
}

function main()
{
    webo.enableLog(__dirname + "/log.txt", "w+");
    var workbook = new WeboBook(__dirname + "/" + global.data);
    var worksheet = workbook.sheet("DATA");
    var realisasiSheet = workbook.sheet("Realisasi");
    
    webo.load(host);
    webo.waitDocumentReady();

    do {
        webo.findElement("#LoginForm_ip").setAttribute("value", webo.arguments.username);
        webo.findElement("#LoginForm_kataSandi").setAttribute("value", webo.arguments.password);
        webo.findElement("button[type=submit]").click();
        webo.waitDocumentReady();

        if (webo.url().indexOf("r=home/index") == -1) {
            webo.log("Login Approweb Gagal!");
            break;
        }

        for (var row = 5; row <= WeboBook.encodeRow(worksheet.maxRow); row++) {
            var number = parseInt(worksheet.value("A" + row));
            if (isNaN(number) || number < parseInt(webo.arguments.start) || number > parseInt(webo.arguments.end))
                continue;

            var npwpFull = worksheet.value("B" + row);
            var npwpFull = worksheet.value("B" + row);
            var noSp2dk = worksheet.value("M" + row);
            var nilaiPotensiAkhir = worksheet.value("T" + row);
            var tahunRealisasi = worksheet.value("U" + row);
            var mapRealisasi = worksheet.value("V" + row);
            var kjsRealisasi = worksheet.value("W" + row);
    
            var errors = [];
            var npwpSearch = npwpFull.replace(/\./g, "").replace(/-/g, "").trim();
            if (!npwpSearch)
                errors.push("NPWP Kosong");
    
            if (!nilaiPotensiAkhir || isNaN(nilaiPotensiAkhir))
                errors.push("Nilai Potensi Akhir invalid");
    
            if (errors.length) {
                webo.log("Nomor " + number + ": " + errors.join(", "));
                continue;
            }
            webo.load(host + "/index.php?r=home/cariWP");
            webo.waitDocumentReady();

            webo.findElement("#CariWPForm_kataCari").setAttribute("value", npwpSearch);
            webo.findElement("input[name=yt0]").click();
            webo.waitDocumentReady();

            var npwpTargetElm;
            webo.findElement("#tabCariWP > div.table-responsive").findElements("tbody tr").forEach(function(tr, index, arr) {
                var tempElm = tr.findElement("a");
                var npwpTarget = tempElm.text().replace(/\./g, "").replace(/-/g, "").trim();
                if (npwpTarget == npwpSearch)
                    npwpTargetElm = tempElm;
            });

            if (!npwpTargetElm) {
                webo.log("Nomor " + number + ": NPWP Tidak ditemukan!");
                continue;
            }
    
            npwpTargetElm.click();
            webo.findElement("input[value='Buka profil']").click();
            webo.waitDocumentReady();
    
            if (webo.url().indexOf("r=permanen/ikhtisarWP") == -1) {
                webo.load(host + "/index.php?r=permanen/ikhtisarWP");
                webo.waitDocumentReady();
            }
    
            var isNpwpOk = false;
            webo.waitForElement(".portlet-body > table:nth-child(1) > tbody:nth-child(1)");
            webo.findElement(".portlet-body > table:nth-child(1) > tbody:nth-child(1)").findElements("tr").forEach(function(tr, index, arr) {
                var tdList = tr.findElements("td");
                if (tdList[0] && tdList[0].text().toLowerCase() == "npwp") {
                    if (tdList[2]) {
                        var npwpTarget = tdList[2].text().replace(/\./g, "").replace(/-/g, "");
                        if (npwpTarget == npwpSearch)
                            isNpwpOk = true;
                    }
                }
            });
    
            if (!isNpwpOk) {
                webo.log("Nomor " + number + ": Gagal membuka Profile!");
                continue;
            }

            webo.load(host + "/index.php?r=analisaData/monitoring");
            webo.waitDocumentReady();
    
            var trFound = 0;
            var table = webo.findElement("#output div.table-scrollable table tbody");
            table.findElements("tr").forEach(function(tr, index, arr) {
                var td = tr.findElements("td");
                if (noSp2dk) {
                    if (!trFound && noSp2dk == td[1].text())
                        trFound = tr;
                }
                else {
                    var nominal = td[td.length - 3].text().replace(/,/g, "");
                    if (!trFound && nilaiPotensiAkhir == nominal)
                        trFound = tr;
                }
            });
    
            if (!trFound) {
                webo.log("Nomor " + number + ": SP2DK tidak ditemukan.");
                continue;
            }
    
            var tdFound = trFound.findElements("td");
            if (tdFound[tdFound.length - 5].text() == "Tgl Kirim belum di input") {
                webo.log("Nomor " + number + ": Pengiriman belum diInput");
                continue;
            }
            else if (tdFound[tdFound.length - 5].text() == "LHP2DK") {
                webo.log("Nomor " + number + ": LHP2DK belum diInput");
                continue;
    
            }
            else if (tdFound[tdFound.length - 5].text() == "Dalam Pengawasan") {
                tdFound[tdFound.length - 2].findElement("a").click();
                webo.waitDocumentReady();
    
                webo.findElement("#output div.table-scrollable table tbody").findElement("a").click();
                webo.waitDocumentReady();
    
                for (var rRow=1; rRow<=WeboBook.encodeRow(realisasiSheet.maxRow); rRow++) {
                    var rNpwp = realisasiSheet.value("A" + rRow);
                    var rMap = realisasiSheet.value("C" + rRow);
                    var rKjs = realisasiSheet.value("D" + rRow);
                    var rTahun = realisasiSheet.value("F" + rRow);
                    var rNtpn = realisasiSheet.value("I" + rRow);
    
                    if (rNpwp && rNpwp.replace(/\./g, "").replace(/-/g, "").trim() != npwpFull.replace(/\./g, "").replace(/-/g, "").trim())
                        continue;
    
                    if (rTahun != tahunRealisasi)
                        continue;
    
                    if (mapRealisasi && rMap.substring(0, mapRealisasi.length) != mapRealisasi)
                        continue;
    
                    if (kjsRealisasi && rKjs.substring(0, kjsRealisasi.length) != kjsRealisasi)
                        continue;
    
                    var found = false;
                    webo.findElement(".table").findElements("tr").forEach(function(tr, index, arr) {
                        var tdList = tr.findElements("td");
                        if (tdList.length > 1 && tdList[1].text() == rNtpn)
                            found = true;
                    });
    
                    if (found) {
                        webo.log("Nomor " + number + ": NTPN " + rNtpn + " sudah ada direalisasi");
                        continue;
                    }
                    
                    webo.findElement("#Sp2dkRealisasi_ntpn").sendKeys(rNtpn);
                    webo.findElement("#ntpn > .form-group > .col-md-1 > .btn> .fa").click();
    
                    if (!webo.waitForElement("#realisasiPilih .btn > .fa", 30000)) {
                        webo.findElement("#webo.-modal > .modal-dialog > .modal-content > .modal-header > .close").click();
                        webo.log("Nomor " + number + ": NTPN " + rNtpn + " timeout saat pencarian ntpn");
                        continue;
                    }
                    webo.findElement("#realisasiPilih .btn > .fa").click();
                    if (!webo.waitForInputCondition("#Sp2dkRealisasi_tgl", "value", "!=", "", 30000)) {
                        webo.log("Nomor " + number + ": NTPN " + rNtpn + " timeout saat load ntpn");
                        continue;
                    }
    
                    webo.findElement("button[type=submit]").click();
                    webo.waitDocumentReady();
    
                    var recorded = false;
                    webo.findElement(".table").findElements("tr").forEach(function(tr, index, arr) {
                        var tdList = tr.findElements("td");
                        if (tdList.length > 1 && tdList[1].text() == rNtpn)
                            recorded = true;
                    });
                    
                    webo.log("Nomor " + number + ": NTPN " + rNtpn + " " + (recorded? "berhasil" : "gagal") + " direkam");
                }
            }
            else {
                webo.log("Nomor " + number + ": Keadaan bukan dalam pengawasan");
            }
        }
    } while (false);

    webo.quit();
}

webo.start(main);